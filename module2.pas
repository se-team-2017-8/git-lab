unit module2;

interface
uses module1;
procedure output(var squareMatrix: Matrix);
procedure swap(var squareMatrix: Matrix);


implementation
procedure output(var squareMatrix: Matrix);
var i,j : integer;
begin
  for i:=1 to 17 do
  begin
      writeln();
      for j:=1 to 17 do
      begin
          write('  ',squareMatrix[i,j]:3) ;
      end;
  end;
  writeln;
  writeln;
end;

procedure swap(var squareMatrix: Matrix);
var minimalElement, minimalNumber, maximalElement, maximalNumber, i, j: integer;
buferMatrix: array[1..17] of integer;
begin
    minimalElement:= squareMatrix[1,1];
    maximalElement:= squareMatrix[1,1];
    for i:= 1 to 17 do
    begin
        for j:= 1 to 17 do
        begin
            if squareMatrix[i,j] < minimalElement  then
            begin
                minimalElement := squareMatrix[i,j];
                minimalNumber:=j;
            end;
            if squareMatrix[i,j] > maximalElement  then
            begin
                maximalElement := squareMatrix[i,j];
                maximalNumber:=j;
            end;
        end;
    end;
    for i:= 1 to 17 do
    begin
        buferMatrix[i]:=squareMatrix[i,minimalNumber];
    end;
    for i:= 1 to 17 do
    begin
        squareMatrix[i,minimalNumber]:=squareMatrix[i,maximalNumber];
    end;
    for i:= 1 to 17 do
    begin
        squareMatrix[i,maximalNumber]:=buferMatrix[i];
    end;
end;

end.

