program TransformationMatrix;
uses module1, module2;
var realMatrix: Matrix;
begin
    CreateMatrix(realMatrix);
    output(realMatrix);
    swap(realMatrix);
    output(realMatrix);
    PutAssortedColumn(realMatrix);
    FindMinElementCollateralDiagonal(realMatrix);
    readln;
end.
