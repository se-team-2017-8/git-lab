unit module1;
interface
type Matrix=array[1..17,1..17] of integer;
procedure CreateMatrix(var squareMatrix: Matrix);
procedure PutAssortedColumn(var squareMatrix: Matrix);
procedure FindMinElementCollateralDiagonal(var squareMatrix: Matrix);

implementation
procedure CreateMatrix(var squareMatrix: Matrix);
var i,j : integer;
begin
   randomize ;
   for i:=1 to 17 do
       begin
       for j:=1 to 17 do
           begin
           squareMatrix[i,j]:=(random(27)-13);
           end;
       end;
end;

procedure PutAssortedColumn(var squareMatrix: Matrix);
var i,j,k,sum : integer;
IsSorted,SortingExists: boolean ;
begin
   sum:= 0;
   SortingExists:=false;
   for j:=1 to 17 do
      begin
      IsSorted:=true;
      for i:=1 to 16 do
         begin
         if (((squareMatrix [i,j])<=(squareMatrix [i+1,j]))) then
            begin
            IsSorted:=false;
            end;
        end;
   if IsSorted = true then
      begin
      SortingExists:=true;
      for k:=1 to 17 do
         begin
         sum:= sum+squareMatrix[k,j];
         end;
      writeln ('number ',j,' sum = ', sum);
      sum:=0;
      end;

      end;
   if (SortingExists=false) then
      begin
      writeln('There are no sorted columns' );
      end;
end;
procedure FindMinElementCollateralDiagonal(var squareMatrix: Matrix);
var i,j,MinElement,ColumnNumberMinimumElement,StringNumberMinimumElements: integer;
begin
   StringNumberMinimumElements:=17;
   ColumnNumberMinimumElement:=1;
   j:=17;
   MinElement:=squareMatrix[17,1];
   for i:=1 to 17 do
   begin
      if(squareMatrix[i,j]<MinElement) then
      begin
         MinElement :=squareMatrix[i,j];
	 ColumnNumberMinimumElement:=j;
	 StringNumberMinimumElements:=i;
      end;
      dec(j);
   end;
   writeln('#string = ', StringNumberMinimumElements ,', #column = ',ColumnNumberMinimumElement,', min element = ',MinElement);
end;

end.




